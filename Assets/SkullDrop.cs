﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullDrop : MonoBehaviour
{
    // Start is called before the first frame update
    public LayerMask LayerPlayer;

    public float LifeTime = 3;

    public Sound SoundPickup;

    private void Update()
    {
        LifeTime -= Time.deltaTime;

        if (LifeTime < 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (((1 << collision.gameObject.layer) & LayerPlayer.value) != 0)
        {
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundPickup, SoundPickup.clip.name);
            FindObjectOfType<ScoreController>().AddScore(100);
            Destroy(this.gameObject);
        }

    }
}
