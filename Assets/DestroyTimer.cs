﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTimer : MonoBehaviour
{

    // Start is called before the first frame update

    public float Lifetime = 2;

    // Update is called once per frame
    void Update()
    {
        Lifetime -= Time.deltaTime;

        if (Lifetime <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
