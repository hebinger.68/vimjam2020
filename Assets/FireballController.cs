﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    bool Exploded = false;

    public LayerMask LayerPlayer;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!Exploded)
        {
            if (((1 << collision.gameObject.layer) & LayerPlayer.value) != 0)
            {
                Exploded = true;

                Destroy(this.gameObject);
            }
        }
    }
}
