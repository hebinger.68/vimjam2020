﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIDeathScreen : MonoBehaviour
{
    // Start is called before the first frame update

    public TextMeshProUGUI Primary;
    public TextMeshProUGUI Secondary;

    private void Start()
    {
        this.GetComponent<Canvas>().enabled = false;
    }

    public void Show()
    {
        this.GetComponent<Canvas>().enabled = true;
    }

    const string HIGHSCORE = "v1-highscore";

    public void SetScore(int score)
    {

        int highscore = 0;

        if (PlayerPrefs.HasKey(HIGHSCORE))
        {
            highscore = PlayerPrefs.GetInt(HIGHSCORE);
        }

        if (score > highscore)
        {
            Primary.text = $"NEW HIGHSCORE !\n{score}";
            Secondary.text = $"Well done !";

            PlayerPrefs.SetInt(HIGHSCORE, score);
        }
        else
        {
            Primary.text = $"SCORE\n{score}";
            Secondary.text = $"HIGHSCORE !\n{highscore}";
        }


    }

    public void RetryButton()
    {
        FindObjectOfType<SceneLoader>().ReloadScene();
    }

}
