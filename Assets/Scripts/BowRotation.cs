﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowRotation : MonoBehaviour
{

    Camera cam;

    public SpriteRenderer Bow;
    public SpriteRenderer Arrow;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 ray = cam.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;

        float angle = Vector2.SignedAngle(Vector2.up, ray);

        transform.rotation = Quaternion.Euler(0, 0, angle);

        if (angle < -90 || angle > 90)
        {
            Bow.sortingOrder = 1;
            Arrow.sortingOrder = 2;
        }
        else
        {
            Bow.sortingOrder = -2;
            Arrow.sortingOrder = -1;
        }

    }
}
