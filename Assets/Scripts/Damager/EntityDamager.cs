﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityDamager : MonoBehaviour
{
    public LayerMask LayerEntity;

    public int DamageOverTime = 1;
    [Space]
    public int DamageOnImpact = 1;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (DamageOverTime == 0) return;

        if (((1 << collision.gameObject.layer) & LayerEntity.value) != 0)
        {

            IEntityDamageable entity = collision.gameObject.GetComponent<IEntityDamageable>();

            entity.DamageEntity(DamageOverTime, true);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (DamageOnImpact == 0) return;

        if (((1 << collision.gameObject.layer) & LayerEntity.value) != 0)
        {

            IEntityDamageable entity = collision.gameObject.GetComponent<IEntityDamageable>();

            entity.DamageEntity(DamageOnImpact, false);

        }
    }
}
