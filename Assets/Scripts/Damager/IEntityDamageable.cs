﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEntityDamageable
{
    // Start is called before the first frame update

    bool IsInvincible();

    void DamageEntity(int damage, bool v);


}
