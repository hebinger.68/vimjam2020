﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour, IEntityDamageable
{
    public bool IsAlive;

    Rigidbody2D body;

    public float Speed = 5;

    public int HealthPoints = 3;
    public int MaxHelthPoints = 3;

    public float DamageInvinsibilityTime = 2;

    HealthDisplay healthDisplay;

    Animator animator;

    public Sound SoundPlayerHurt;
    public Sound SoundPlayerDie;

    public void AddHealth(int v)
    {

        HealthPoints += v;

        if (HealthPoints > MaxHelthPoints)
        {
            HealthPoints = MaxHelthPoints;
        }

        healthDisplay.UpdateDisplay(HealthPoints);

    }

    // Start is called before the first frame update
    void Start()
    {
        IsAlive = true;
        body = GetComponent<Rigidbody2D>();
        healthDisplay = GetComponent<HealthDisplay>();
        animator = GetComponent<Animator>();
        timeAtLastDamage = -DamageInvinsibilityTime;

        healthDisplay.UpdateDisplay(HealthPoints);

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsAlive)
        {
            Vector2 playerInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));


            body.velocity = playerInput * Speed;


            Vector2 pointing = Camera.main.ScreenToWorldPoint(Input.mousePosition) - this.transform.position;
            animator.SetFloat("MovementX", pointing.x);
            animator.SetFloat("MovementY", pointing.y);
            animator.SetFloat("Speed", playerInput.sqrMagnitude);

        }

        animator.SetBool("Hurt", IsInvincible());
    }


    float timeAtLastDamage = 0;

    public void DamageEntity(int amount, bool timed)
    {
        if (!IsInvincible())
        {
            //Debug.Log("Player damaged " + amount);

            timeAtLastDamage = Time.time;

            HealthPoints -= amount;

            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundPlayerHurt, SoundPlayerHurt.clip.name);

            if (HealthPoints <= 0)
            {
                PlayerDeath();
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundPlayerDie, SoundPlayerDie.clip.name);
            }

            healthDisplay.UpdateDisplay(HealthPoints);
        }
    }

    public bool IsInvincible()
    {
        return !(Time.time > (timeAtLastDamage + DamageInvinsibilityTime));
    }

    public void PlayerDeath()
    {
        Debug.Log("Player died");

        StartCoroutine(DeathScreen());

    }

    IEnumerator DeathScreen()
    {
        yield return new WaitForSeconds(1);

        FindObjectOfType<UIDeathScreen>().Show();

        FindObjectOfType<UIDeathScreen>().SetScore(FindObjectOfType<ScoreController>().Score);

        Time.timeScale = 0;

        this.gameObject.SetActive(false);
    }


}
