﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingEnemy : EnemyDamagable
{

    public float Speed = 5;

    Rigidbody2D body;
    SpriteRenderer renderer;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        body = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        body.velocity = VectorToPlayer().normalized * Speed;

        renderer.flipX = (body.velocity.x > 0);
    }

    public override void EnemyDeath()
    {
        FindObjectOfType<ScoreController>().AddScore(10);
        body.velocity = Vector2.zero;
        animator.Play("enemy_head_death");
        base.EnemyDeath();
    }
}
