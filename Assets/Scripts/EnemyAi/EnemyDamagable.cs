﻿using System.Collections;
using UnityEngine;

public class EnemyDamagable : EnemyBase, IEntityDamageable
{

    public bool IsAlive;

    public int HealthPoints = 5;
    public int MaxHelthPoints = 5;

    public float DamageInvinsibilityTime = 2;

    protected Animator animator;

    public GameObject [] drops;

    protected Transform grid;

    public Sound SoundSkullDamaged;
    public Sound SoundSkullDeath;

    // Start is called before the first frame update
    public new void Start() {
        base.Start();
        IsAlive = true;
        animator = this.GetComponent<Animator>();

        grid = FindObjectOfType<Grid>().transform;
    }

    float timeAtLastDamage = 0;
    public void DamageEntity(int amount, bool timed) {
        if (!IsInvincible() || !timed) {
            if (timed) {
                timeAtLastDamage = Time.time;
            }

            HealthPoints -= amount;



            if (HealthPoints <= 0) {
                EnemyDeath();
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundSkullDeath, SoundSkullDeath.clip.name);
            } else {
                animator.Play("enemy_hurt_base");
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundSkullDamaged, SoundSkullDamaged.clip.name);
            }
        }
    }

    public bool IsInvincible() {
        return !(Time.time > (timeAtLastDamage + DamageInvinsibilityTime));
    }

    public virtual void EnemyDeath() {
        IsAlive = false;

        Debug.Log("Enemy died");
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
        Destroy(GetComponentInChildren<EntityDamager>().gameObject);

        StartCoroutine(DestroyEntity());

        if (Random.Range(0, 3) == 0) {
            Instantiate(drops [Random.Range(0, drops.Length)], this.transform.position, Quaternion.identity, grid);
        }

    }

    IEnumerator DestroyEntity() {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }


}
