﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : EnemyDamagable
{

    public float Speed = 5;

    Rigidbody2D body;
    SpriteRenderer renderer;

    public GameObject FireBall;

    public float FireDelay = 2;
    float FireCooldown = 0;

    public float FireballSpeed = 5;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        body = GetComponent<Rigidbody2D>();
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        body.velocity = VectorToPlayer().normalized * Speed;

        renderer.flipX = (body.velocity.x > 0);

        FireCooldown += Time.deltaTime;

        if (FireCooldown > FireDelay && IsAlive)
        {
            FireCooldown -= FireDelay;

            GameObject fireballInstance = Instantiate(FireBall, this.transform.position, Quaternion.identity, grid);
            fireballInstance.GetComponent<Rigidbody2D>().velocity = VectorToPlayer().normalized * FireballSpeed;
        }

    }

    public override void EnemyDeath()
    {
        FindObjectOfType<ScoreController>().AddScore(10);
        body.velocity = Vector2.zero;
        animator.Play("enemy_head_death_red");
        base.EnemyDeath();
    }
}
