﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour
{

    public GameObject prefab;

    public float SpawnDelay = 10;

    private float Cooldown = 0;

    public float RateOverTime = 0.02f;

    // Update is called once per frame
    void Update()
    {
        Cooldown += Time.deltaTime;
        if (Cooldown > (SpawnDelay - Time.time * RateOverTime))
        {

            Instantiate(prefab, transform.position, Quaternion.identity, FindObjectOfType<Grid>().transform);

            Cooldown -= (SpawnDelay - Time.time * RateOverTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(this.transform.position, 0.3f);
    }
}
