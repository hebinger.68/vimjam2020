﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{

    protected PlayerController player;

    // Start is called before the first frame update
    public void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    public Vector2 VectorToPlayer()
    {
        if (player != null)
        {
            return player.transform.position - this.transform.position;
        }
        else
        {
            return Vector2.zero;
        }


    }

}
