﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public bool IsPaused = true;

    public TextMeshProUGUI display;

    public float time = 0;



    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsPaused)
        {
            time += Time.unscaledDeltaTime;
        }

        display.SetText(this.ToString());
    }

    public void ResetTimer()
    {
        time = 0;
    }


    public override string ToString()
    {
        return TimeToString(time);
    }

    public static string TimeToString(float time)
    {
        int millis = (int)((time * 10000) % 10000);
        int seconds = (int)time;
        int minutes = seconds / 60;

        int secondsCut = seconds - minutes * 60;

        string secondsString = secondsCut.ToString("D2");
        string millisString = millis.ToString("D4");

        return string.Format($"{minutes}:{secondsString}:{millisString}");
    }

}
