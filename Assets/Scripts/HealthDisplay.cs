﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{

    public Image [] Images;

    public Sprite HealthFull;
    public Sprite HealthEmpty;


    public void UpdateDisplay(int healthpoints)
    {
        for (int i = 0; i < Images.Length; i++)
        {
            if (i < healthpoints)
            {
                Images [i].sprite = HealthFull;
            }
            else
            {
                Images [i].sprite = HealthEmpty;
            }

        }


    }


}
