﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ArrowSpawner : MonoBehaviour
{

    public GameObject ArrowPrefab;
    public GameObject BombArrowPrefab;

    public int Arrows = 5;

    public TextMeshProUGUI display;

    public int MaxArrows = 5;


    public SpriteRenderer ArrowDisplay;
    public SpriteRenderer BowDisplay;

    public Image BombDisplay;

    public Sound SoundArrowShoot;
    public Sound SoundArrowBombShoot;

    [Space]

    public Sprite ArrowSprite;
    public Sprite BombSprite;
    public Sprite BowLoaded;
    public Sprite BowUnloaded;

    public bool NextIsBomb = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (Arrows > 0 && Input.GetMouseButtonUp(0))
        {

            if (NextIsBomb)
            {
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundArrowBombShoot, SoundArrowBombShoot.clip.name);
                Instantiate(BombArrowPrefab, this.transform.position, this.transform.rotation, FindObjectOfType<Grid>().transform);
                NextIsBomb = false;
            }
            else
            {
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundArrowShoot, SoundArrowShoot.clip.name);
                Instantiate(ArrowPrefab, this.transform.position, this.transform.rotation, FindObjectOfType<Grid>().transform);
            }

            Arrows -= 1;



            if (Arrows == 0)
            {
                ArrowDisplay.sprite = null;
                BowDisplay.sprite = BowUnloaded;
            }
            else
            {
                if (NextIsBomb)
                {
                    ArrowDisplay.sprite = BombSprite;
                }
                else
                {
                    ArrowDisplay.sprite = ArrowSprite;
                    BombDisplay.color = new Color(0, 0, 0, 0);
                }

            }
        }

        display.text = $"{Arrows}/{MaxArrows}";

    }

    public void SetNextBomb(bool next)
    {
        NextIsBomb = next;

        if (next)
        {
            ArrowDisplay.sprite = BombSprite;
            BombDisplay.color = new Color(1, 1, 1, 1);
        }

    }

    public void AddArrow(int i)
    {
        Arrows += i;

        if (Arrows > MaxArrows)
        {
            Arrows = MaxArrows;
        }

        if (NextIsBomb)
        {
            ArrowDisplay.sprite = BombSprite;
            BombDisplay.color = new Color(1, 1, 1, 1); ;
        }
        else
        {
            ArrowDisplay.sprite = ArrowSprite;
            BombDisplay.color = new Color(0, 0, 0, 0); ;
        }
        BowDisplay.sprite = BowLoaded;
    }
}
