﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{

    public LayerMask LayerPlayer;
    public LayerMask LayerObstacle;

    public bool IsStuck;

    Rigidbody2D body;

    public float Speed = 10;

    public Sound sound;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        IsStuck = false;

        float angle = this.transform.rotation.eulerAngles.z + 90;
        body.velocity = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad)) * Speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsStuck)
        {
            if (((1 << collision.gameObject.layer) & LayerPlayer.value) != 0)
            {
                AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sound, sound.clip.name);
                collision.gameObject.GetComponentInChildren<ArrowSpawner>().AddArrow(1);
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (((1 << collision.gameObject.layer) & LayerObstacle.value) != 0)
            {
                IsStuck = true;
                body.velocity = Vector2.zero;
                Destroy(GetComponent<EntityDamager>());
            }
        }
    }
}
