﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{

    public RoomSpawner [] SpawnRooms;
    public RoomSpawner [] LevelRooms;

    public int Depth = 1;

    public enum DoorsDirection
    {
        Up, Down, Left, Right
    }


    public Dictionary<KeyValuePair<int, int>, RoomSpawner> rooms;

    public List<DoorsDirection> path;

    public Vector2Int FinalRoom;

    public bool IsReady = false;

    void Start()
    {
        path = new List<DoorsDirection>();
        rooms = new Dictionary<KeyValuePair<int, int>, RoomSpawner>();


        System.Random random = new System.Random();

        RoomSpawner spawnRoom = SpawnRooms [Random.Range(0, SpawnRooms.Length)];

        rooms.Add(new KeyValuePair<int, int>(0, 0), spawnRoom);

        Instantiate(spawnRoom.gameObject);

        StartCoroutine(GenerateMap(spawnRoom));
    }

    IEnumerator GenerateMap(RoomSpawner spawnRoom)
    {
        while (!GenerateRandomMainRooms(Depth, Vector2Int.zero, spawnRoom.doors))
        {
            path = new List<DoorsDirection>();
            rooms = new Dictionary<KeyValuePair<int, int>, RoomSpawner>();

            rooms.Add(new KeyValuePair<int, int>(0, 0), spawnRoom);

        }

        IsReady = true;

        yield return null;

    }


    public bool GenerateRandomMainRooms(int depth, Vector2Int position, DoorsDirection lastDoor)
    {
        position += DirectionToVector(lastDoor);

        path.Add(lastDoor);
        if (depth == 0)
        {
            RoomSpawner thisRoom = FindRoom(OpositeDoor(lastDoor), lastDoor);
            rooms.Add(new KeyValuePair<int, int>(position.x, position.y), thisRoom);

            FinalRoom = position;
            return true;
        };

        depth--;


        if (rooms.ContainsKey(new KeyValuePair<int, int>(position.x, position.y)))
        {
            Debug.LogError("room error !");
            return false;
        }

        int security = 0;

        while (true)
        {
            security++;

            if (security > 100)
            {
                return false;
            }

            DoorsDirection nextDoor = RandomDoor();

            if (nextDoor == OpositeDoor(lastDoor))
            {
                continue;
            }

            Vector2Int nextPosition = position + DirectionToVector(nextDoor);

            if (rooms.ContainsKey(new KeyValuePair<int, int>(nextPosition.x, nextPosition.y)))
            {
                continue;
            }


            RoomSpawner thisRoom = FindRoom(nextDoor, lastDoor);
            rooms.Add(new KeyValuePair<int, int>(position.x, position.y), thisRoom);

            return GenerateRandomMainRooms(depth, position, nextDoor);
        }

        //render path
    }


    public DoorsDirection RandomDoor()
    {
        Array values = Enum.GetValues(typeof(DoorsDirection));
        return (DoorsDirection)values.GetValue(Random.Range(0, values.Length));
    }

    public Vector2Int DirectionToVector(DoorsDirection direction)
    {
        switch (direction)
        {
            case DoorsDirection.Up:
                return Vector2Int.up;
            case DoorsDirection.Down:
                return Vector2Int.down;
            case DoorsDirection.Left:
                return Vector2Int.left;
            case DoorsDirection.Right:
                return Vector2Int.right;
            default:
                return Vector2Int.up;

        }
    }

    public DoorsDirection OpositeDoor(DoorsDirection direction)
    {
        switch (direction)
        {
            case DoorsDirection.Up:
                return DoorsDirection.Down;
            case DoorsDirection.Down:
                return DoorsDirection.Up;
            case DoorsDirection.Left:
                return DoorsDirection.Right;
            case DoorsDirection.Right:
                return DoorsDirection.Left;
            default:
                return DoorsDirection.Up;

        }
    }

    public RoomSpawner FindRoom(DoorsDirection nextDoor, DoorsDirection lastDoor)
    {
        while (true)
        {
            RoomSpawner room = LevelRooms [Random.Range(0, LevelRooms.Length)];

            if ((room.doors | nextDoor | OpositeDoor(lastDoor)) == room.doors)
            {
                return room;
            }
        }


    }


}
