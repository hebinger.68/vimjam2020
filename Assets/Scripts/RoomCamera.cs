﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RoomCamera : MonoBehaviour
{
    Camera cam;

    public Vector2 RoomSize;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        float screenRatio = (float)Screen.width / (float)Screen.height;
        float targetRatio = RoomSize.x / RoomSize.y;

        if (screenRatio >= targetRatio)
        {
            cam.orthographicSize = RoomSize.y / 2;
        }
        else
        {
            float differenceInSize = targetRatio / screenRatio;
            cam.orthographicSize = RoomSize.y / 2 * differenceInSize;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Vector3.zero + this.transform.position, RoomSize);
    }
}
