﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Display.text = $"{this.Score}";
    }

    public TextMeshProUGUI Display;

    public int Score = 0;

    public void AddScore(int score)
    {
        this.Score += score;

        Display.text = $"{this.Score}";
    }

}
