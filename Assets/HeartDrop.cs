﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartDrop : MonoBehaviour
{
    // Start is called before the first frame update

    public LayerMask LayerPlayer;

    public Sound SoundPickup;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (((1 << collision.gameObject.layer) & LayerPlayer.value) != 0)
        {
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundPickup, SoundPickup.clip.name);
            FindObjectOfType<PlayerController>().AddHealth(1);
            Destroy(this.gameObject);
        }

    }

}
