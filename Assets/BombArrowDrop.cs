﻿using UnityEngine;

public class BombArrowDrop : MonoBehaviour
{
    public LayerMask LayerPlayer;

    public Sound SoundPickup;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (((1 << collision.gameObject.layer) & LayerPlayer.value) != 0)
        {
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundPickup, SoundPickup.clip.name);
            FindObjectOfType<ArrowSpawner>().AddArrow(1);
            FindObjectOfType<ArrowSpawner>().SetNextBomb(true);

            Destroy(this.gameObject);
        }

    }
}
