﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombArrowController : MonoBehaviour
{
    // Start is called before the first frame update

    public LayerMask LayerExplode;

    private bool Exploded;

    public GameObject Explosion;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!Exploded)
        {
            if (((1 << collision.gameObject.layer) & LayerExplode.value) != 0)
            {
                Exploded = true;

                Instantiate(Explosion, this.transform.position, Quaternion.identity, FindObjectOfType<Grid>().transform);

                Destroy(this.gameObject);
            }
        }
    }

}
